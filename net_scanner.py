import scapy.all as scapy

#1)arp_request
#2)broadcast
#3)response

arp_request_packet=scapy.ARP(pdst="192.168.56.0/24")
arp_broadcast_packet=scapy.Ether(dst="ff:ff:ff:ff:ff:ff") #default olarak modeme gidiyor bu mac adresiyle
combined_packet= arp_broadcast_packet/arp_request_packet  #bu iki methodu birleştirip tek paketle gönderiyor
(answered,unanswered)=scapy.srp(combined_packet,timeout=1)
answered.summary()
